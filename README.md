# Drone-acharya

Drone-Acharya: Realtime UAV Detection, Monitoring and Authentication

### How to Run

#### Setup PostgreSQL

- `sudo apt-get install postgresql postgresql-client postgis-*`
- `sudo service postgresql start` to start service
- `psql --host=localhost  --username=postgres` (If username/password differs, update in `project_constants.py`)
- `create database dronacharya;`

#### Setup virtualenv

- `pip install virtualenv`
- `virtualenv venv` (In project directory)
- `source venv/bin/activate` 

#### Setup Dependencies

- `pip install -r requirements.txt`

#### Start!
- `FLASK_APP=app.py flask run`
- Visit `http://localhost:5000` in browser to see Drone-Acharya in action
- The simulator is accessible at `http://localhost:5000/simulator`
- To reset at any point simply visit `http://localhost:5000/reset` and it removes all old drone data
