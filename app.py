from flask import Flask, render_template, request
import psycopg2, json
from psycopg2.pool import SimpleConnectionPool
from contextlib import contextmanager

import project_constants

app = Flask(__name__)


dbConnection = "dbname='dronacharya' user='" + project_constants.CONSTANTS["USER"] + "' host='localhost' password='" + project_constants.CONSTANTS["PASS"] + "'"

# pool define with 10 live connections
connectionpool = SimpleConnectionPool(1,10,dsn=dbConnection)

@contextmanager
def getconn():
    con = connectionpool.getconn()
    try:
        yield con
    finally:
        connectionpool.putconn(con)

@app.route("/")
def hello():
    return render_template('index.html')

@app.route("/simulate")
def simulate():
    return render_template('simulation.html')


@app.route("/insert", methods=['POST'])
def insert():
    with getconn() as con:
        did = request.form.get('did')
        ts = request.form.get('timestamp')
        llong = request.form.get('llong')
        llat = request.form.get('llat')
        authorised = request.form.get('authorised')
        sql = "INSERT INTO drone_data(drone_id, ts, authorised,  geom) VALUES ('%s', TO_TIMESTAMP('%s','Dy,  DD Mon YYYY HH24:MI:SS GMT'), %s, ST_GeomFromText('POINT(%s %s)', 24374));" % (did, ts, authorised, llong, llat)
        con.cursor().execute(sql)
        con.commit()
        return "{\"status\" : \"update_success\"}"

@app.route("/getDrones", methods=['POST'])
def getDrones():
    with getconn() as con:
        include_all = request.form.get('include_all')
        authorised_filter = ""
        if include_all != "true":
            authorised_filter = " WHERE authorised != True "
        sql = "select distinct on (drone_id) drone_id, authorised, ts, ST_AsText(geom) from drone_data %s order by drone_id, ts desc" % (authorised_filter);
        cursor = con.cursor()
        cursor.execute(sql)
        rows = cursor.fetchall()
        returnvals = []
        for row in rows:
            returnvals.append({"id": row[0], "ts": row[2], "authorised": row[1], "geom": row[3]})
           
        return json.dumps(returnvals, default=str)

@app.route("/detectIntrusion", methods=['POST'])
def detectIntrusion():
    with getconn() as con:
        sql = "select distinct on (drone_id) drone_id, ST_AsText(geom) from drone_data WHERE authorised != True  order by drone_id, ts desc"
        cursor = con.cursor()
        cursor.execute(sql)
        rows = cursor.fetchall()
        check_status = []
        for row in rows:
            check_status.append((row[0], row[1]))
        intrusions = []
        for check in check_status:
            sql = "SELECT ST_INTERSECTS(geom, '"+ check[1] +"') from polygon"
            cursor = con.cursor()
            cursor.execute(sql)
            row = cursor.fetchone()
            if row[0]:
                intrusions.append(check[0])
           
        return json.dumps(intrusions, default=str)

@app.route("/getDronePaths", methods=['POST'])
def getDronePaths():
    with getconn() as con:
        include_all = request.form.get('include_all')
        authorised_filter = ""
        if include_all != "true":
            authorised_filter = " WHERE authorised != True "
        sql = "select drone_id, ts, ST_Y(geom), ST_X(geom) from drone_data %s order by drone_id desc, ts asc" % (authorised_filter);
        cursor = con.cursor()
        cursor.execute(sql)
        rows = cursor.fetchall()
        returnvals = [{}]
        for row in rows:
            if row[0] not in returnvals[0]:
                returnvals[0][row[0]] = []
            returnvals[0][row[0]].append([row[3], row[2]])
        return json.dumps(returnvals, default=str)

@app.route("/reset")
def setup():
    with getconn() as con:
        con.cursor().execute("DROP TABLE IF EXISTS drone_data")
        
        con.cursor().execute('''CREATE TABLE drone_data
          (ID SERIAL PRIMARY KEY NOT NULL,
           drone_id CHAR(10) NOT NULL,
           ts timestamp NOT NULL,
           authorised BOOLEAN NOT NULL,
           geom GEOMETRY(Point, 24374) NOT NULL);''')
        con.cursor().execute("DELETE FROM polygon")
        con.cursor().execute("INSERT INTO polygon (name, geom) VALUES ('dcpgn', ST_GeomFromGeoJSON('{\"type\": \"Polygon\", \"coordinates\": [[[77.689227, 12.953552],[ 77.688989, 12.949170],[ 77.678610, 12.948151],[ 77.666723, 12.946189],[ 77.661837, 12.946904],[77.651373, 12.951394],[ 77.660992, 12.952884],[ 77.668018, 12.958057],[ 77.680024, 12.954441],[ 77.664595, 12.955004],[77.668018, 12.958057],[ 77.672406, 12.957816],[ 77.680024, 12.954441],]]}'));")
        con.commit()


        return "{\"status\" : \"success\"}"